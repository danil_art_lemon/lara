@extends('layouts.app')
@section('content')
<h1>hello blog</h1>
<div class="row">
    @foreach($blogs as $blog)
        <br>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <a href="{{ route('blog_path', ['blog'=>$blog->id]) }}">
                        {{$blog->title}}
                    </a>

                </div>
                
                <div class="card-body">
                    <img src="{{ asset($blog->image) }}" alt="" class="card-img-top">
                    {{ print_r($blog->content) }}
                    <br>
                    <br>

                    <p class="lead">
                        Posted:
                        {{ $blog->created_at->diffForHumans() }}
                    </p>
                    <a href="{{ route('blog_path', ['blog' => $blog->id]) }}" class="btn btn-outline-primary">View Post</a>
                </div>
            </div>
        </div>
    @endforeach
</div>

@endsection