@extends('layouts.app')
@section('content')
    <form action="{{ route('update_blog_path' , ['blog' => $blog->id]) }}" method="post" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="title">Title</label>
            <input type="text" class="form-control" name="title" value="{{$blog->title}}">
        </div>
        <img src="{{ asset($blog->image) }}" alt="" class="card-img-top">
        <div class="form-group">
            <input type="file" name="files" class="form-control" value="{{ $blog->image }}">
        </div>
        <div class="form-group">
            <label for="content">Comment</label>
            <textarea type="text" rows="10" class="form-control" name="content">{{$blog->content }}</textarea>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-outline-primary">Edit</button>
        </div>
    </form>
@endsection