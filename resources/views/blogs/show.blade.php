@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <h1>{{$blog->title}}</h1>
            <p class="lead">
                {{$blog->content}}
            </p>
            <a href="{{ route('blogs_path') }}" class="btn btn-outline-secondary">Back</a>
            <a href="{{ route('edit_blog_path', ['blog' => $blog->id]) }}" class="btn btn-outline-info">Edit</a>
            <form action="{{ route('delete_blog_path', ['blog' => $blog->id]) }}" method="POST">
                @csrf
                @method('delete')
                <button type="submit" class="btn btn-outline-danger" >Delete</button>

            </form>
        </div>

    </div>
@endsection