@extends('layouts.app')
@section('content')
    <form action="{{ route('store_blog_path') }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label for="title">Title</label>
            <input type="text" class="form-control" name="title">
        </div>
        <div class="form-group">
            <label for="content">Comment</label>
            <textarea type="text" rows="10" class="form-control" name="content"></textarea>
        </div>

        <div class="form-group">
            <input type="file" name="files" class="form-control">
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-outline-primary">Add</button>
        </div>
    </form>
@endsection